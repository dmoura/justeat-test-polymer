(function(document) {
    'use strict';

    var app = document.querySelector('#app');

    app.config = {
        api: {
            endpoint: 'https://public.je-apis.com',
            authorization: 'Basic VGVjaFRlc3RBUEk6dXNlcjI=', //refactor to be smarter
            language: 'en-GB',
            tenant: 'uk'
        }
    };
})(document);
