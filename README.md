# Just Eat: Recruitment Test (Polymer WebApp)
This is a submission to the Just-Eat recruitment test.
This is a technology demo of PolymerJs a WebComponents library by Google.

A live copy of this project is hosted [here](http://justeat-recruitment-diogo.s3-website-eu-west-1.amazonaws.com/).

## Installing dependencies
This project uses `npm`, `wct`, `bower` and `gulp`, once npm has been installed please run the following to install the other dependencies.

```
npm install -g bower gulp-cli  web-component-tester
npm install
bower install
```

## Development
To run a local development server for the project please use gulp:

```
gulp serve
```

## Tests
Unit tests in this project are ran with web-components-tester via gulp.

```
gulp test
```

## Production
We can use gulp to produce a production ready package.

```
gulp
```

## Notes
* css flex box model is not compatible with older browsers.
* css vendor prefixes intentionally omitted, cross browser support was not a focus of this demo.


## Known Issues

* Outcode is over-ridden when HTML5 getCurrentPosition returns with a delay.
 Issue is usually not present on devices with a dedicated GPS.
